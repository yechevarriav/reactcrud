import {
    ADD_USER_SUCCESS,
    FETCH_USER_SUCCESS,
    FETCH_USERS_SUCCESS,
    FILTER_USERS,
    REMOVE_USER_SUCCESS
} from '../constants/action-types';

const initialState = {
    items: [],
    item: {},
    filtered: []
};

export default (state = initialState, { type, payload }) => {
    switch(type) {
        case FETCH_USERS_SUCCESS:
            return { ...state, items: payload, item: {}, filtered: [] };
        case FETCH_USER_SUCCESS:
            return { ...state, item: payload };
        case ADD_USER_SUCCESS:
            return { ...state, items: [...state.items, payload ] };
        case REMOVE_USER_SUCCESS:
            return { ...state, items: state.items.filter(user =>user.id !== payload) }
        case FILTER_USERS:
            const value = payload.toLowerCase();
            return { ...state, filtered: state.items.filter(({ name }) => {
                return name.toLowerCase().includes(value);
            })};
        default:
            return state;
    }
};
