import React, { Component } from 'react'

class SearchUsers extends Component {

    state = {
        searchValue: ''
    };

    handleSubmit = e => {
        e.preventDefault();
        this.props.handleSearchUsers(this.state.searchValue);
    };

    handleChange = e => {
        this.setState({ searchValue: e.target.value });
    };

    render() {
        const { searchValue } = this.state;

        return (
            <form onSubmit={this.handleSubmit} style={{ margin: '1.5rem 0.3rem' }}>
                <div className="field has-addons">
                    <div className="control">
                        <input
                            className="input"
                            type="text"
                            value={searchValue}
                            placeholder="Buscar Usuario"
                            onChange={this.handleChange}
                        />
                    </div>
                    <div className="control">
                        <input type="submit" value="Buscar" className="button is-info" />
                    </div>
                </div>
            </form>
        );
    }
}




export default SearchUsers;