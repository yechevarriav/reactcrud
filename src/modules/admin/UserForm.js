import React from 'react'

import { Formik } from 'formik';
import * as yup from 'yup';

const userSchema = yup.object({
    name: yup.string().required(),
    lastname: yup.string().required(),
    email: yup.string().email().required(),
    role: yup.string().required(),
    password: yup.string().required(),
    phone: yup.string().required().min(7)
});

export default ({ title, user, submit, back }) => {
    return (
        <div className="container">
            <div className="columns">
                <div className="column is-8 is-offset-2">
                    <Formik
                        initialValues={user}
                        validationSchema={userSchema}
                        onSubmit={(values) => {
                            submit(values);
                        }}
                    >
                        {({
                            values,
                            isValid,
                            errors,
                            touched,
                            handleBlur,
                            handleChange,
                            handleSubmit
                        }) => (
                            <form action="" noValidate autoComplete="off">
                                <div className="field">
                                    <div className="control">
                                        <input
                                            className={ errors.name && touched.name ? 'input is-danger' : 'input' }
                                            type="text"
                                            name="name"
                                            value={values.name}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            placeholder=" Ingrese Nombres "
                                        />
                                        {
                                            errors.name && touched.name
                                            ? <p className="help is-danger">El nombre es obligatorio</p>
                                            : null
                                        }
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <input
                                            className={ errors.lastname && touched.lastname ? 'input is-danger' : 'input' }
                                            type="text"
                                            name="lastname"
                                            value={values.lastname}
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            placeholder=" Ingresar Apellidos "
                                        />
                                        {
                                            errors.lastname && touched.lastname
                                            ? <p className="help is-danger">Los Apellidos son obligatorios</p>
                                            : null
                                        }
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <input
                                            className={ errors.email && touched.email ? 'input is-danger' : 'input' }
                                            type="text"
                                            name="email"
                                            value={values.email}
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            placeholder="Email"
                                        />
                                        {
                                            errors.email && touched.email
                                            ? <p className="help is-danger">El email es obligatorio y de formato correcto</p>
                                            : null
                                        }
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">

                                      <select value={values.role} name="role"
                                      className={ errors.role && touched.role ? 'input is-danger' : 'input' }
                                      onBlur={handleBlur}
                                      onChange={handleChange}
                                      > 
                                      <option value="">Seleccione..</option>
                                            <option value="admin">admin</option>
                                            <option value="user">user</option>
                                        </select>
                                      
                                        {
                                            errors.role && touched.role
                                            ? <p className="help is-danger">Debes seleccionar un role</p>
                                            : null
                                        }
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <input
                                            className={ errors.password && touched.password ? 'input is-danger' : 'input' }
                                            type="password"
                                            name="password"
                                            value={values.password}
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            placeholder="Ingresar Clave"
                                        />
                                        {
                                            errors.password && touched.password
                                            ? <p className="help is-danger">Ingresar la clave</p>
                                            : null
                                        }
                                    </div>
                                </div>
                                <div className="field">
                                    <div className="control">
                                        <input
                                            className={ errors.phone && touched.phone ? 'input is-danger' : 'input' }
                                            type="text"
                                            name="phone"
                                            value={values.phone}
                                            onBlur={handleBlur}
                                            onChange={handleChange}
                                            placeholder="Ingresar Teléfono"
                                        />
                                        {
                                            errors.phone && touched.phone
                                            ? <p className="help is-danger">Ingresar número telefónico</p>
                                            : null
                                        }
                                    </div>
                                </div>
                                <div className="field is-grouped is-grouped-centered">
                                    <p className="control">
                                        <button
                                            type="button"
                                            className="button"
                                            onClick={() => back() }
                                        >Regresar</button>
                                    </p>
                                    <p className="control">
                                        <button
                                            type="button"
                                            onClick={handleSubmit}
                                            className="button is-dark"
                                            disabled={!isValid}
                                        >{ title }</button>
                                    </p>
                                </div>
                            </form>
                        )}
                    </Formik>
                </div>
            </div>
        </div>
    );

}