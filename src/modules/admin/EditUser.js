import React, { Component } from 'react'
import { connect } from 'react-redux';
import {updateUser,fetchUser} from '../../actions/userActions';
import UserForm from './UserForm';

class EditUser extends Component{

    componentDidMount() {
        const {params :{id}} = this.props.match;
        this.props.fetchUser(id);
    }

    handleSubmit = user =>{
        this.props.updateUser(user);
        this.goBack();
    }

    goBack = () => {
        this.props.history.goBack();
    }

    render() {
        const { user } = this.props;
        return(
            <div className="columns">
                <div className="column is-10 is-offset-1">
                <h2 className="subtitle is-3 has-text-centered">Actualizar Usuario</h2>
                  {
                      user.id ?
                      <UserForm
                      title="Actualizar"
                      user={user}
                      submit={this.handleSubmit}
                      back={this.goBack}
                      />
                      : null
                  }
                </div>

           </div>
        );

    }


}

export default connect((state)=>{

    return{
        user: state.users.item
    };

},{updateUser,fetchUser})(EditUser);