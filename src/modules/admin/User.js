import React, { Fragment } from 'react'
import { Link } from 'react-router-dom';

const User = ({ user , index, handleDelete }  ) =>{
    const {id, name, lastname, email, role, password , phone } = user;
  
    return(
       
        <Fragment>
            <tr>
                <td className="has-text-grey" > { index + 1 } </td>
                <td className="has-text-grey" > { name } </td>
                <td className="has-text-grey" >{ lastname }</td>
                <td className="has-text-grey" >{ role }</td>
                <td className="has-text-grey" >{ email }</td>
                <td className="has-text-grey" >{ phone }</td>

                <td>
                    <button className="button is-small is-danger" onClick={() => { handleDelete(id) }}>
                        Eliminar
                    </button>
                    
                    <Link className="button is-small is-info" to={`users/${id}`}>
                        Editar
                    </Link>
                </td>

            </tr>

        </Fragment>

    );

};

export default User;