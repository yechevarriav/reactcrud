import React, { Component } from 'react'
import { connect } from 'react-redux';

import UserForm from './UserForm';
import { addUser } from '../../actions/userActions';

class NewUser extends Component {
    handleSubmit = user => {
        this.props.addUser(user);
        this.goBack();
    }

    goBack = () => {
        this.props.history.goBack();
    }


    render() {
        const user = {
            name: '', lastname: '',  email: '',  role: '',
            password: '',  phone: ''  }


        return (
            <div className="columns" >
                <div className="column is-10 is-offset-1">
                    <h2 className="subtitle is-3 has-text-centered">Crear Usuario</h2>
                    <UserForm
                        title="Crear"
                        user = {user}
                        submit={this.handleSubmit}
                        back={this.goBack}
                     />
                </div>
            </div>

        )
    };
}
export default connect(null, { addUser })(NewUser);